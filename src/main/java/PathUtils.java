import java.nio.file.Paths;

public class PathUtils {
    static String join(String first, String... more) {
        return Paths.get(first, more).toString();
    }
}
