import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.*;

import java.io.IOException;
import java.util.Iterator;

class UserRating {

    private static class Map extends MapReduceBase implements
            Mapper<LongWritable, Text, LongWritable, Text> {
        @Override
        public void map(LongWritable key, Text value,
                        OutputCollector<LongWritable, Text> output,
                        Reporter reporter) throws IOException {
            String[] tokens = value.toString().split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)");

            Long userId = Long.parseLong(tokens[0].trim());
            Integer movieId = Integer.parseInt(tokens[1].trim());
            Double rating = Double.parseDouble(tokens[2].trim());

            output.collect(new LongWritable(userId), new Text(movieId + "|" + rating));
        }
    }

    private static class Reduce extends MapReduceBase implements
            Reducer<LongWritable, Text, LongWritable, Text> {
        @Override
        public void reduce(LongWritable key, Iterator<Text> values,
                           OutputCollector<LongWritable, Text> output, Reporter reporter)
                throws IOException {
            String output_value = "";
            while (values.hasNext()) {
                output_value += values.next().toString();
                if (values.hasNext()) output_value += ",";
            }

            output.collect(key, new Text(output_value));
        }
    }

    static void run(String input_folder, String output_folder) throws Exception  {
        // Parse all the genres
        JobConf conf = new JobConf(KMeans.class);

        conf.setJobName("UserRating");
        conf.setMapOutputKeyClass(LongWritable.class);
        conf.setMapOutputValueClass(Text.class);
        conf.setOutputKeyClass(LongWritable.class);
        conf.setOutputValueClass(Text.class);
        conf.setMapperClass(Map.class);
        conf.setReducerClass(Reduce.class);
        conf.setInputFormat(TextInputFormat.class);
        conf.setOutputFormat(TextOutputFormat.class);

        String input_file = PathUtils.join(input_folder, "ratings.csv");
        FileInputFormat.setInputPaths(conf, new Path(input_file));
        FileOutputFormat.setOutputPath(conf, new Path(output_folder));

        JobClient.runJob(conf);
    }

}
