import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.*;


public class KMeans {

    private static Integer NUM_CENTROIDS = 5;
    private static ArrayList<ArrayList<Double>> CENTROIDS = new ArrayList<>();
    private static HashMap<Integer, Integer> MOVIES_INDEX = new HashMap<>();

    private static class Map extends MapReduceBase implements
            Mapper<LongWritable, Text, IntWritable, Text> {
        @Override
        public void configure(JobConf job) {
            String movies_path = job.get("movies_path");
            String centroids_path = job.get("centroids_path");
            try {
                MOVIES_INDEX = ParseMoviesIndex(movies_path);
                CENTROIDS = ParseCentroids(centroids_path);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void map(LongWritable key, Text value,
                        OutputCollector<IntWritable, Text> output,
                        Reporter reporter) throws IOException {
            String data_str = value.toString();
            String[] DATA = data_str.split("\t| ");

            Integer user_id = Integer.parseInt(DATA[0]);
            String[] pair_movie_ratings = DATA[1].split(",");

            ArrayList<Double> point = new ArrayList<>(Collections.nCopies(MOVIES_INDEX.size(), 0d));

            for(String pair_str : pair_movie_ratings) {
                String[] pair = pair_str.split("\\|");

                Integer movie_id = Integer.parseInt(pair[0]);
                Double rating = Double.parseDouble(pair[1]);

                point.set(MOVIES_INDEX.get(movie_id), rating);
            }

            Integer centroid_index = 0;
            Double min_distance = Double.MAX_VALUE;
            for (int j = 0; j < CENTROIDS.size(); j++) {
                Double total = 0d;
                for (int i = 0; i < MOVIES_INDEX.size(); i++) {
                    total += (point.get(i) - CENTROIDS.get(j).get(i)) *
                             (point.get(i) - CENTROIDS.get(j).get(i));
                }
                if (total < min_distance) {
                    min_distance = total;
                    centroid_index = j;
                }
            }

            // Convert the point to a string representation
            String point_str = "";
            for (int j = 0; j < MOVIES_INDEX.size(); j++) {
                point_str += String.format("%.4f", point.get(j));
                if (j != MOVIES_INDEX.size() - 1) point_str += ",";
            }

            output.collect(new IntWritable(centroid_index), new Text(user_id + "|" + point_str));
        }
    }

    private static class Reduce extends MapReduceBase implements
            Reducer<IntWritable, Text, Text, Text> {
        @Override
        public void reduce(IntWritable key, Iterator<Text> values,
                           OutputCollector<Text, Text> output, Reporter reporter)
                throws IOException {

            Integer num_users = 0;
            String users_str = "";

            ArrayList<Double> new_centroid = new ArrayList<>(Collections.nCopies(MOVIES_INDEX.size(), 0d));

            while (values.hasNext()) {
                String[] DATA = values.next().toString().split("\\|");
                Integer user_id = Integer.parseInt(DATA[0]);
                String[] points_str = DATA[1].split(",");

                for (int i = 0; i < points_str.length; i++) {
                    Double val = Double.parseDouble(points_str[i]);
                    new_centroid.set(i, new_centroid.get(i) + val);
                }

                num_users++;
                users_str += user_id;
                if (values.hasNext()) users_str += ",";
            }

            for (int i = 0; i < new_centroid.size(); i++) {
                new_centroid.set(i, new_centroid.get(i) / num_users);
            }

            // Convert the point to a string representation
            String centroid_str = "";
            for (int j = 0; j < MOVIES_INDEX.size(); j++) {
                centroid_str += String.format("%.4f", new_centroid.get(j));
                if (j != MOVIES_INDEX.size() - 1) centroid_str += ",";
            }

            output.collect(new Text(key + "|" + centroid_str), new Text(users_str));
        }

        @Override
        public void close() throws IOException {

        }
    }

    private static ArrayList<ArrayList<Double>> ParseCentroids(String centroids_path) throws Exception {
        String sCurrentLine;
        FileSystem fs = FileSystem.get(new Configuration());
        Path pt = new Path(centroids_path);
        BufferedReader br = new BufferedReader(new InputStreamReader(fs.open(pt)));

        ArrayList<ArrayList<Double>> centroids = new ArrayList<>(
            Collections.nCopies(NUM_CENTROIDS, null)
        );

        while ((sCurrentLine = br.readLine()) != null) {
            String[] centroids_str = sCurrentLine.split("\t| ")[0].split("\\|");
            Integer index = Integer.parseInt(centroids_str[0]);
            String[] values_str = centroids_str[1].split(",");
            ArrayList<Double> tmp = new ArrayList<>();
            for(String centroid : values_str) {
                tmp.add(Double.parseDouble(centroid));
            }
            centroids.set(index, tmp);
        }

        br.close();

        return centroids;
    }

    private static void DumpCentroids(String output_path, ArrayList<ArrayList<Double>> centroids) throws Exception {
        FileSystem fs = FileSystem.get(new Configuration());
        Path pt = new Path(output_path);
        BufferedWriter br = new BufferedWriter(new OutputStreamWriter(fs.create(pt, true)));

        for (int i = 0; i < centroids.size(); i++) {
            String line = String.valueOf(i) + "|";
            ArrayList<Double> tmp = centroids.get(i);
            for (int j = 0; j < tmp.size(); j++) {
                line += String.format("%.4f", tmp.get(j));
                if (j != tmp.size() - 1) line += ",";
            }
            br.write(line);
            br.newLine();
        }

        br.close();
    }

    private static HashMap<Integer, Integer> ParseMoviesIndex(String movies_path) throws Exception {
        String sCurrentLine;
        FileSystem fs = FileSystem.get(new Configuration());
        Path pt = new Path(movies_path);
        BufferedReader br = new BufferedReader(new InputStreamReader(fs.open(pt)));
        br.readLine();  // Ignore the csv header

        Integer index = 0;
        HashMap<Integer, Integer> result = new HashMap<>();
        while ((sCurrentLine = br.readLine()) != null) {
            String[] DATA = sCurrentLine.split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
            Integer MovieID = Integer.parseInt(DATA[0]);
            result.put(MovieID, index++);
        }

        br.close();

        return result;
    }

    private static void GenerateInitialCentroids(Integer num_movies,
                                                 String centroids_path) throws Exception {
        FileSystem fs = FileSystem.get(new Configuration());
        Path pt = new Path(centroids_path);
        BufferedWriter br = new BufferedWriter(new OutputStreamWriter(fs.create(pt, true)));

        Random rnd = new Random();

        for (int i = 0; i < NUM_CENTROIDS; i++) {
            String line = String.valueOf(i) + "|";
            for (int j = 0; j < num_movies; j++) {
                line += String.format("%.4f", rnd.nextDouble() * 5.0);
                if (j != num_movies - 1) line += ",";
            }
            br.write(line);
            br.newLine();
        }

        br.close();
    }

    private static Integer CountFileLines(String file_path) throws IOException {
        FileSystem fs = FileSystem.get(new Configuration());
        Path pt = new Path(file_path);
        BufferedReader reader = new BufferedReader(new InputStreamReader(fs.open(pt)));
        int lines = 0;
        while (reader.readLine() != null) lines++;
        reader.close();
        return lines;
    }

    public static void main(String[] args) throws Exception {
        Locale.setDefault(Locale.US);
        run(args);
    }

    private static void run(String[] args) throws Exception {
        if (args.length < 2) {
            System.out.println("Usage: KMeans input output");
            return;
        }

        String DEFAULT_OUTPUT_FILENAME = "part-00000";

        DateFormat df = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
        Date today = Calendar.getInstance().getTime();
        String running_date = df.format(today);

        String input_base = args[0];
        String output_base = PathUtils.join(args[1], running_date);

        String output_ratings = PathUtils.join(output_base, "ratings");
        UserRating.run(input_base, output_ratings);

        String input_file = PathUtils.join(output_ratings, DEFAULT_OUTPUT_FILENAME);
        String output_folder = PathUtils.join(output_base, "0");

        // Movies and Centroids files location
        String centroids_path = PathUtils.join(input_base, "centroids.txt");
        String movies_path = PathUtils.join(input_base, "movies.csv");

        Integer num_movies = CountFileLines(movies_path) - 1;

        System.out.print("Generating centroids... ");
        GenerateInitialCentroids(num_movies, centroids_path);
        System.out.println("Done.");

        // Reiterating till the convergence
        int iteration = 0;
        while (true) {
            System.out.println("Starting Iteration " + iteration);

            JobConf conf = new JobConf(KMeans.class);

            conf.setJobName("KMeans");
            conf.setMapOutputKeyClass(IntWritable.class);
            conf.setMapOutputValueClass(Text.class);
            conf.setOutputKeyClass(Text.class);
            conf.setOutputValueClass(Text.class);
            conf.setMapperClass(Map.class);
            conf.setReducerClass(Reduce.class);
            conf.setInputFormat(TextInputFormat.class);
            conf.setOutputFormat(TextOutputFormat.class);

            // Store some variables to use in the map-reduce process
            conf.set("movies_path", movies_path);
            conf.set("centroids_path", centroids_path);

            FileInputFormat.setInputPaths(conf, new Path(input_file));
            FileOutputFormat.setOutputPath(conf, new Path(output_folder));

            JobClient.runJob(conf);

            // Save the last centroids file
            String old_centroids_path = centroids_path;

            // Count the number of the centroids in the output
            String out_centroids_path = PathUtils.join(output_folder, DEFAULT_OUTPUT_FILENAME);
            Integer num_output_centroids = CountFileLines(out_centroids_path);

            if (!Objects.equals(num_output_centroids, NUM_CENTROIDS)) {
                ArrayList<ArrayList<Double>> out_centroids = ParseCentroids(out_centroids_path);

                Random rnd = new Random();

                for (int i = 0; i < NUM_CENTROIDS; i++) {
                    ArrayList<Double> centroid = out_centroids.get(i);
                    if (centroid == null) {
                        ArrayList<Double> tmp = new ArrayList<>();
                        for (int j = 0; j < num_movies; j++) {
                            tmp.add(rnd.nextDouble() * 5.0);
                        }
                        out_centroids.set(i, tmp);
                    }
                }

                System.out.println("[Iteration " + iteration + "] Outlier centroids");

                centroids_path = PathUtils.join(output_folder, "centroids.txt");
                DumpCentroids(centroids_path, out_centroids);
            } else {
                ArrayList<ArrayList<Double>> new_centroids = ParseCentroids(out_centroids_path);
                ArrayList<ArrayList<Double>> old_centroids = ParseCentroids(old_centroids_path);

                Double Total_RMSE = 0d;
                for (int i = 0; i < new_centroids.size(); i++) {
                    ArrayList<Double> new_centroid = new_centroids.get(i);
                    ArrayList<Double> old_centroid = old_centroids.get(i);

                    Double RMSE = 0d;
                    for (int j = 0; j < new_centroid.size(); j++) {
                        RMSE += (new_centroid.get(j) - old_centroid.get(j)) *
                                (new_centroid.get(j) - old_centroid.get(j));
                    }
                    RMSE = Math.sqrt(RMSE / new_centroid.size());
                    Total_RMSE += RMSE;
                }

                Total_RMSE /= new_centroids.size();

                System.out.println("[Iteration " + iteration + "] Root-mean-square error: " + Total_RMSE);
                if (Total_RMSE <= 0.1d) {
                    break;
                }

                centroids_path = out_centroids_path;
            }

            iteration++;
            output_folder = PathUtils.join(output_base, String.valueOf(iteration));
        }
    }
}

